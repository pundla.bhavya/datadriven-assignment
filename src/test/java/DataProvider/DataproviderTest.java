package DataProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
public class DataproviderTest {

	
	WebDriver driver;  
	@BeforeClass
	public void setup() {
	    driver=new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	}    @Test (dataProvider="LoginData")
	public void loginTest(String username, String password) {       
		driver.get("https://demowebshop.tricentis.com/");       
		driver.findElement(By.linkText("Log in")).click();
	    
	    driver.findElement(By.id("Email")).sendKeys(username);
	    driver.findElement(By.id("Password")).sendKeys(password);
	    driver.findElement(By.xpath("//input[@value='Log in']")).click();
	    driver.findElement(By.linkText("Log out")).click();  
	    
				}   

		  
	@DataProvider(name="LoginData")
	public String [][] getData() throws IOException {
		
		  File f= new File("/home/pundla/Documents/DataDriven/DataProver.xlsx");
		  FileInputStream fis= new FileInputStream(f);
		  XSSFWorkbook workbook=new XSSFWorkbook(fis);
		  XSSFSheet sheet=workbook.getSheetAt(0);
		  int norows=sheet.getPhysicalNumberOfRows();
		  int noColumns=sheet.getRow(0).getLastCellNum();
		  String data[][] = new String[norows-1][noColumns];
		  for(int i=1;i<norows;i++)
		  {
			  for(int j=0; j<noColumns; j++)
			  {
				  data[i-1][j] =sheet.getRow(i).getCell(j).getStringCellValue();
				  }
			  }
		  return data;
		 }
		 }
		
		

