package RegistrationData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisterDataUsingJxl {

	public static void main(String[] args) throws BiffException, IOException {
		// TODO Auto-generated method stub
		
WebDriver driver =new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/register");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		File f=new File("/home/pundla/Documents/DataDriven/testdata1.xls");
		FileInputStream fis= new FileInputStream(f);
		
		Workbook book=Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("RegisterData");
		String celldata=sh.getCell(1, 1).getContents();
		System.out.println("Value Present is:" +celldata);
		
		int rowCount= sh.getRows();
		int columnsCount=sh.getColumns();
		for(int i=1;i<rowCount;i++)
		{
			String firstname=sh.getCell(0,1).getContents();
			String lastname=sh.getCell(1,1).getContents();
			String email=sh.getCell(2,1).getContents();
			String password=sh.getCell(3,1).getContents();
			
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
			driver.findElement(By.id("register-button")).click();
			System.out.println("Registeration Completed");
		}
		
	}


	}



